$Id$ 

-- SUMMARY --

The Bugtracker module is a simple tool to log issues during the developement process of a website. It has
his own data scheme so it will not interfere with any other modules neither appear in node management or search engine.

Bugs can be logged, viewed, commented, deleted, and updated.

Each bug can be associated with a priority level and a type of bug. Updates are logged to see the
progression of the bug.

A list of emails can be set to recieve a notification each time a new bug is logged. 
 

-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure user permissions: admin/user/permissions

  - Register a bug
    To gain access to the Bugtracker panel to log a bug.
	
  - Delete and Update Bug
    To gain access to deleting or updating already logged bug.
	Only users with this permission can delete comments on bug.
	
  - View logged bugs
    To gain acces to the bug list and the single bug page. A user with this permission can also
    comment a bug, see others comment and see the log of changes on the bug. 
	
  - Configure Bugtracker module
    Can access to the bugtracker configuration page. 
	
* Configure email notifications: admin/reports/bugtracker/settings

  - On this page you can set a list of recipients for email notifications when new bugs are logged. 


-- CONTACT --

Current maintainers:
* Matthieu Gadrat - matthieu@symetris.ca
* David Pinard - david@symetris.ca


This project has been sponsored by:
* SYMETRIS
  Specialized in web site developement.
  http://www.symetris.ca/

