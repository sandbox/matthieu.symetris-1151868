﻿<?php // $Id$ 

// Module Bugtracker
class Bugtracker_model {
  
  // Properties //
   
  // Constructor //
  public function __construct() {
    
    } 

  // Methods //

  public function get_bug_priority_list(){
    
    return $this->get_bug_classification_term('priority');
    
    }
    
  public function get_bug_type_list(){
    
    return $this->get_bug_classification_term('type');
    
    }
    
  public function get_bug_status_list(){
    
    return $this->get_bug_classification_term('status');
    
    }
    
  public function get_single_bug($bid){
    
    $bug_array = $this->load_bugs($bid);
    return $bug_array[0];
    
    }
    
  public function get_bug_list($sort = 'timestamp',$sort_order = 'down'){
    
    return $this->load_bugs(NULL, $sort,$sort_order);
    
    }
    
  public function create_single_bug($bug,$primary_key = NULL){
    
    if($primary_key){
      $this->log_changes($bug);
      }
    
    if(drupal_write_record('bugtracker_bugs', $bug,$primary_key)){
      return $bug;
      }else{
        return FALSE;
        }
    
    }
   
  public function create_single_bug_comment($comment){
        
    return drupal_write_record('bugtracker_bug_comments', $comment);
    
    }
    
  public function delete_single_comment($cid){
        
    $q = "
    DELETE 
    FROM {bugtracker_bug_comments}
    WHERE id = '%d'";
    
    $result = db_query($q,$cid);
      
    return true;
  }

  public function delete_single_bug($bid){
        
    $q = "
    DELETE 
    FROM {bugtracker_bugs}
    WHERE bid = '%d'";
    
    $result = db_query($q,$bid);
    
    // Delete related comments
    $q = "
    DELETE 
    FROM {bugtracker_bug_comments}
    WHERE bid = '%d'";
    
    $result = db_query($q,$bid);
    
    return true;
  }
  
  public function get_bug_comments($bid){
    
    $q = "
    SELECT
    {bugtracker_bug_comments}.id    AS 'id',
    {bugtracker_bug_comments}.bid    AS 'bid',
    {bugtracker_bug_comments}.timestamp  AS 'timestamp',
    {bugtracker_bug_comments}.w_name  AS 'w_name',
    {bugtracker_bug_comments}.comment  AS 'comment'
    
    FROM {bugtracker_bug_comments} 

    WHERE bid = '%d'
    ORDER BY 'timestamp' DESC
    ";
        
    $result = db_query($q,$bid);
    
    $rows = array();
    while ($row = db_fetch_array($result)){
      
      $rows[] = $row;
      
      }
    
    return $rows;
    
    }
    
  public function get_bug_log($bid){
    
    $q = "
    SELECT
    {bugtracker_log_msgs}.id    AS 'id',
    {bugtracker_log_msgs}.bid    AS 'bid',
    {bugtracker_log_msgs}.timestamp  AS 'timestamp',
    {bugtracker_log_msgs}.message  AS 'message'
    
    FROM {bugtracker_log_msgs} 

    WHERE bid = '%d'
    ORDER BY 'timestamp' DESC
    ";
        
    $result = db_query($q,$bid);
    
    $rows = array();
    while ($row = db_fetch_array($result)){
      
      $row['message'] = unserialize($row['message']);
      
      $rows[] = $row;
      
      }
    
    return $rows;
    
    }
    
  protected function get_bug_classification_term($realm = NULL){

    $where_clause = "";
    if($realm){
      $where_clause = "WHERE realm = '%s'";
      }

    $q = "
    SELECT
    {bugtracker_classification_terms}.id,
    {bugtracker_classification_terms}.term
    
    FROM {bugtracker_classification_terms} 
    
    $where_clause 
    
    ORDER BY weight";
    
    $terms = array();
    $result = db_query($q,$realm);
    
    while ($row = db_fetch_array($result)) {
      $terms[$row['id']] = $row['term'];
      }
    
    
    return $terms;
    }
  
  protected function load_bugs($bid = NULL,$sort = 'timestamp', $sort_order = 'down'){
    
    $where_clause = "";
    $limit_clause = "";
    $sort = db_escape_string($sort);
    
    // For single bug
	// is_int prevents injection 
    if($bid && is_int($bid)){
      $where_clause = "WHERE bid = '$bid'";
      $limit_clause = "LIMIT 1";
      }
    
    // For sort  
    if($sort_order == 'up'){
      $sort_order = 'ASC';
      }else{
        $sort_order = 'DESC';
        }

    if($sort == ''){$sort = 'timestamp';}
	
    $sort_clause = 'ORDER BY '.db_escape_string($sort).' '.$sort_order;
    
    $q = "
    SELECT
    {bugtracker_bugs}.bid AS 'bid',
    {bugtracker_bugs}.timestamp AS 'timestamp',
    {bugtracker_bugs}.title AS 'title',
    {bugtracker_bugs}.w_name AS 'w_name',
    {bugtracker_bugs}.w_email AS 'w_email',
    {bugtracker_bugs}.w_ip AS 'w_ip',
    {bugtracker_bugs}.w_user_agent AS 'w_user_agent',
    {bugtracker_bugs}.description AS 'description',
    {bugtracker_bugs}.location AS 'location',
    {bugtracker_bugs}.drupal_uid AS 'drupal_uid',
    {bugtracker_bugs}.priority_fk AS 'priority_fk',
    {bugtracker_bugs}.type_fk AS 'type_fk',
    {bugtracker_bugs}.status_fk AS 'status_fk',
    
    priority_tab.term AS 'priority',
    priority_tab.weight AS 'priority_weight',
    
    type_tab.term AS 'type',
    type_tab.weight AS 'type_weight',
    
    status_tab.term AS 'status',
    status_tab.weight AS 'status_weight'
    
    FROM {bugtracker_bugs} 
    
    LEFT JOIN {bugtracker_classification_terms} AS priority_tab
    ON {bugtracker_bugs}.priority_fk = priority_tab.id
    
    LEFT JOIN {bugtracker_classification_terms} AS type_tab
    ON {bugtracker_bugs}.type_fk = type_tab.id
    
    LEFT JOIN {bugtracker_classification_terms} AS status_tab
    ON {bugtracker_bugs}.status_fk = status_tab.id
    
    $where_clause 
    $sort_clause
    $limit_clause
    ";
        
    $result = db_query($q);
    
    $rows = array();
    while ($row = db_fetch_array($result)){
      
      $rows[] = $row;
      
      }
    
    return $rows;
    
    }
  
  protected function log_changes($bug){
    
    
    $oldbug = $this->get_single_bug($bug['bid']);
    $terms = $this->get_bug_classification_term();
    
    $changes = array();
        
      
    foreach($bug as $key => $val){
      
      if($oldbug[$key] && $oldbug[$key] != $val){
        
        $changes[] = array(
          'field' => $key,
          'oldval' => $oldbug[$key],
          'newval' => $val,
          );
        
        }
      
      }
      
    $log = serialize($changes);  
    
    $log = array(
      'bid' => $bug['bid'],
      'timestamp' => time(),
      'message' => $log,
      );
    
    drupal_write_record('bugtracker_log_msgs',$log);
    
    }
  
  public function get_nice_log_message($item){
    
    $terms = $this->get_bug_classification_term();
    
    $fkfields = array('priority_fk', 'type_fk', 'status_fk');
    
    if(in_array($item['field'],$fkfields) ){
      
      $item['oldval'] = $terms[$item['oldval']];
      $item['newval'] = $terms[$item['newval']];
      
      }
  
    $item['field'] = $this-> get_nice_fields_name($item['field']);
    return($item);
    
    }
  
  // Associate a description with each field
  protected function get_nice_fields_name($field){
    
    $fields_name = array(
      'bid' => t('the unique ID of the bug'),
      'timestamp' => t('the date the bug was logged'),
      'title' => t('the title of the bug'),
      'w_name' => t('the name of the witness'),
      'w_email' => t('the email address of the witness'),
      'w_ip' => t('the IP of the witness'),
      'w_user_agent' => t('the user agent of the witness'),
      'description' => t('the description of the bug'),
      'location' => t('the url where the bug was logged'),
      'drupal_uid' => t('the unique ID of the drupal user who logged the bug'),
      'priority_fk' => t('the priority of the bug'),
      'type_fk' => t('the type of the bug'),
      'status_fk' => t('the status of the bug'),
      'priority' => t('the priority of the bug'),
      'priority_weight' => t('the weight of the priority of this bug'),
      'type' => t('the type of the bug'),
      'type_weight' => t('the weight of the type of this bug'),
      'status' => t('the status of the bug'),
      'status_weight' => t('the weight of the status of this bug'),
      );
    
    if($fields_name[$field]) {
      return $fields_name[$field];
      }else{
        return $field;
        }
    
    }
  
}