// $Id$ 

$(document).ready(function(){
						   
/* Hook_footer */
	var boxwidth = $('#bugtracker_box .box_form').outerWidth();
	$('#bugtracker_box').css('right','-'+boxwidth+'px');
	
	$('#bugtracker_box .box_button').toggle(function() {
		
		$('#bugtracker_box').animate({right:0},'fast');
		$(this).addClass('opened');
		
		}, function() {
		
		$('#bugtracker_box').animate({right: -1*boxwidth+'px' },'fast');
		$(this).removeClass('opened');
		
		});

/* Copy account INfos */

	$('.copy_account_infos').click(function(){
											
		$('#edit-w-name').val($('#edit-drupal-name').val());
		$('#edit-w-email').val($('#edit-drupal-email').val());
		
		});

/* Comments & logs */
	$('#bugtracker_bug h4.openable.closed').parent().find('.innerwrap').hide();

	$('#bugtracker_bug h4.openable').click(function() {
		$(this).parent().find('.innerwrap').slideToggle('fast');
		$(this).toggleClass('closed');
	})

	if($.browser.msie && $.browser.version == 7.0){
		
		$('#bugtracker_bug #bugtracker_comments *').addClass('bt_ie7');
		$('#bugtracker_bug #bugtracker_logs *').addClass('bt_ie7');
	
		}

	
	
});