<?php // $Id$ ?>
<div id='bugtracker_bug'>
  <div id="bugtracker_bug_data">
  <h3>Bug #<?php echo $bug_bid ?> - <?php echo $bug_title ?></h3>
  <div class='logged_date'>Logged on <?php echo $bug_date ?></div>
  
  <div class='bugtracker_w_infos'>
  <h4>Witness info</h4>
  <div class='innerwrap'>
  
  <dl>
  <dt>Name: </dt><dd><?php echo $bug_w_name ?></dd>
  <dt>Email address: </dt><dd><?php echo $bug_w_email ?></dd>
  <dt>Drupal UID: </dt><dd><?php echo $bug_drupal_uid ?></dd>
  <dt>Location on the site (URL): </dt><dd><?php echo $bug_location ?></dd>
  <dt>User agent: </dt><dd><?php echo $bug_w_user_agent ?></dd>
  <dt>IP address: </dt><dd><?php echo $bug_w_ip ?></dd>
  </dl>
  </div>
  </div>
    
  <div class='bugtracker_classification_infos'>
    <dl>
      <dt>Priority: </dt><dd><?php echo $bug_priority ?></dd>
      <dt>Type: </dt><dd><?php echo $bug_type ?></dd>
      <dt>Status: </dt><dd><?php echo $bug_status ?></dd>
    </dl>
  </div>
  
  <div id='bugtracker_bug_description'>
    <h4 class='openable'>Description</h4>
      <div class='innerwrap'>
      <p><?php echo $bug_description ?></p>
      </div>
    </div>
  </div>

  <?php if($bug_comments): ?>
  <div id="bugtracker_comments">
    <h4 class='openable'>Comments</h4>
    <div class='innerwrap'>
    <?php echo $bug_comments ?>
    </div>
  </div>
  <?php endif; ?>
  
  <?php if($bug_log_msgs): ?>
  <div id="bugtracker_logs">
    <h4 class='openable closed'>Log of changes on this bug</h4>
    <div class='innerwrap'>
    <?php echo $bug_log_msgs ?>
    </div>
  </div>
  <?php endif; ?>
  
  <div>
    <h4 id="bugtracker_commentform">Comment this bug</h4>
    <div class='innerwrap'>
    <?php echo $bug_commentform ?>
    </div>
  </div>
</div>
