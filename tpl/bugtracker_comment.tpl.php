<?php // $Id$ ?>
<div class='bugtracker_comment'>
  <span class="delete"><?php echo $del_button ?></span>
  <div class="metadata">
    <span class="author"><?php echo $author ?></span>,
    <span class="date"><?php echo $date ?></span>
  </div>
  <p><?php echo $comment ?></p>
  
</div>
