<?php // $Id$ ?>
<div id="bugtracker_box">
    <div class="box_button">
      &nbsp;
    </div>
    <div class="box_form">
      <div class="subtitle"><?php echo $subtitle ?></div>
      <?php echo $form ?>
      <span class="footer"><?php echo $extra_link ?></span>
    </div>
  </div>